FROM amazoncorretto:21-alpine-jdk
VOLUME /tmp
EXPOSE 8080
COPY target/restaurant-0.0.1-SNAPSHOT.jar restaurant.jar
ENTRYPOINT ["java","-jar","/restaurant.jar"]
