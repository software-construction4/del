# Restaurant

# Регистрация admin
Адрес http://localhost:8080/admin/register
Secret Key -  123456

# Описание
1) Добавлять товары могут только администраторы. Чтобы это сделать они могут нажать на кнопку "Добавить товар" в главном меню.
2) После этого они смогут редактировать цену, количество и время приготовления товара в главном меню.
3) Удалять товары следует только в том случае, когда пользователь не заказал уже этот товар. Неправильное удаление может привести к ошибке.
4) Пользовтели могут добавлять товары в корзину, используя главное меню, изменять количество товара в корзине. Также могут сделать заказ нажав на "Купить" в корзине.
5) В разделе заказов пользователь видит номер своего заказа, статус его ожидания. Он может отменить его до того времени, пока он приготовится. После приготовления заказа пользователь может оплатить его.

# Изменяемые начальные параметры 
В application.properties можно изменить максимальное количество заказов, которые могут выполнять одновремено - поле maxOrdersCount, по умолчанию 10.


## Для запуска

```
cd existing_repo
git remote add origin https://gitlab.com/software-construction4/del.git
git branch -M main
git push -uf origin main
docker compose build
docker compose up
```

