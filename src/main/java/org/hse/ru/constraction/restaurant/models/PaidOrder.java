package org.hse.ru.constraction.restaurant.models;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hse.ru.constraction.restaurant.models.enums.Status;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "paid_order")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaidOrder {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id",columnDefinition = "BIGINT")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "status")
    private Status status;

    @Column(name = "unique_number_of_order")
    private String uniqueNumberOfOrder;

    @Column(name = "started_time")
    private LocalDateTime startedTime;

    @Column(name = "created_time")
    private LocalDateTime createdTime;

    @OneToMany(mappedBy = "paidOrder",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    private List<PaidOrderDetails> paidOrderDetails = new LinkedList<>();


    @Transactional
    public int getTotalSum(){
        int sum = 0;
        for(PaidOrderDetails details : paidOrderDetails){
            sum += details.getCount() * details.getProduct().getPrice();
        }
        return sum;
    }

    @Transactional
    public int getTotalTimCooking(){
        int totalTimeCooking = 0;
        for(PaidOrderDetails details : paidOrderDetails){
            totalTimeCooking += details.getCount() * details.getProduct().getTimeCookingMinutes();
        }
        return totalTimeCooking;
    }


}


