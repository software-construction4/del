package org.hse.ru.constraction.restaurant.services;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class UniqueNumberGenerator {
    public String generateRandomNumberOfOrder(){
        Random random = new Random();
        int randomInt = random.nextInt(2601);
        int letterCode = randomInt / 100;
        int number = randomInt % 100;
        char letter = (char) ('A' + letterCode);
        return String.format("%c%02d", letter, number);
    }
}
