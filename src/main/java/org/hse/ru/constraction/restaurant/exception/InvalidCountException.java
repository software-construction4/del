package org.hse.ru.constraction.restaurant.exception;

public class InvalidCountException extends Exception {
    public InvalidCountException(String message) {
        super(message);
    }
}