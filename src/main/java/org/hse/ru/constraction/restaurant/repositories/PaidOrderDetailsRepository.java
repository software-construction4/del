package org.hse.ru.constraction.restaurant.repositories;

import jakarta.transaction.Transactional;
import org.hse.ru.constraction.restaurant.models.CartProduct;
import org.hse.ru.constraction.restaurant.models.PaidOrder;
import org.hse.ru.constraction.restaurant.models.PaidOrderDetails;
import org.hse.ru.constraction.restaurant.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface  PaidOrderDetailsRepository   extends JpaRepository<PaidOrderDetails, Long> {

}
