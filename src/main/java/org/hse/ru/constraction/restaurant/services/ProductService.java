package org.hse.ru.constraction.restaurant.services;

import io.imagekit.sdk.exceptions.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hse.ru.constraction.restaurant.dto.ProductCreateDTO;
import org.hse.ru.constraction.restaurant.dto.UpdateProductDTO;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.repositories.CategoryRepository;
import org.hse.ru.constraction.restaurant.repositories.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final CartProductService cartProductService;
    private final UploadImageService uploadImageService;
    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }
    public boolean createProduct(ProductCreateDTO product) throws ForbiddenException, TooManyRequestsException, InternalServerException, UnauthorizedException, BadRequestException, UnknownException, IOException {
        Product newProduct = new Product(product);
        String path =  uploadImageService.uploadImage(product.getImageFile());
        log.info(path);
        if (product.imageFile.getSize() != 0){
            newProduct.setImageUrl(path);
            newProduct.getCategory().addProduct(newProduct);
            productRepository.save(newProduct);
            log.info("Saving new product with title:{}", newProduct.getTitle());
            return true;
        }
        return false;
    }



    public void updateProduct(UpdateProductDTO productDTO){
        Product product = getProductById(productDTO.getId());
        product.setCount(productDTO.getCount());
        product.setTimeCookingMinutes(productDTO.getTimeCookingMinutes());
        product.setPrice(productDTO.getPrice());
        productRepository.save(product);
    }
    public void deleteProduct(Long productId){
        productRepository.deleteById(productId);
        cartProductService.deleteRelatedProducts(productId);
    }
    public void saveProduct(Product product){
        productRepository.save(product);
    }
    public Product getProductById(Long id){

        return productRepository.findById(id).orElse(null);
    }

    public Product getProductByTitle(String title){
        return productRepository.findByTitle(title);
    }

    public boolean isUniqueProduct(String productName){
        return productRepository.findByTitle(productName) == null;
    }





}
