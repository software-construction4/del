package org.hse.ru.constraction.restaurant.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.hse.ru.constraction.restaurant.models.PaidOrder;
import org.hse.ru.constraction.restaurant.models.PaidOrderDetails;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.models.enums.Status;

import java.util.LinkedList;
import java.util.List;
@Data
@AllArgsConstructor
public class OrderDetailDTO {
    public PaidOrder order;
    public int totalSum;
}
