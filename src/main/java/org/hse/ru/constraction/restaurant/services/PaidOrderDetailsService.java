package org.hse.ru.constraction.restaurant.services;


import lombok.AllArgsConstructor;
import org.hse.ru.constraction.restaurant.models.*;
import org.hse.ru.constraction.restaurant.repositories.PaidOrderDetailsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PaidOrderDetailsService {
    private final CartProductService cartProductService;
    private final PaidOrderDetailsRepository payOrderDetailsRepository;
    private final ProductService productService;
    public void transferDataFromCartToOrder(PaidOrder order, Cart cart){
        List<CartProduct> cartProducts = cartProductService.getCartProductsByCartId(cart.getId());
        cartProducts.forEach(cartProduct -> {
            PaidOrderDetails paidOrderDetails = PaidOrderDetails.builder()
                    .paidOrder(order)
                    .product(productService.getProductById(cartProduct.getProduct_id()))
                    .count(cartProduct.getCount())
                    .build();
            Product product =  productService.getProductById(cartProduct.getProduct_id());
            product.setCount(product.getCount() - cartProduct.getCount());
            productService.saveProduct(product);
            payOrderDetailsRepository.save(paidOrderDetails);
        });
    }

}
