package org.hse.ru.constraction.restaurant.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static jakarta.persistence.GenerationType.IDENTITY;


@Entity
@Data
@Builder
@Table(name = "category")
@AllArgsConstructor
@NoArgsConstructor
public class Category implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;



    @OneToMany( mappedBy = "category",fetch = FetchType.EAGER,orphanRemoval = true)
    @JsonIgnore
    private List<Product> products = new LinkedList<>();
    public void addProduct(Product product){
        products.add(product);
    }
}