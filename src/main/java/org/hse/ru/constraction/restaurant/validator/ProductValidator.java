package org.hse.ru.constraction.restaurant.validator;

import lombok.AllArgsConstructor;
import org.hse.ru.constraction.restaurant.dto.ProductCreateDTO;
import org.hse.ru.constraction.restaurant.services.ProductService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Component
@AllArgsConstructor
public class ProductValidator {
    private final ProductService productService;
    public boolean  validateProduct(Model model,BindingResult bindingResult, ProductCreateDTO productCreateDTO){
        boolean correct = !bindingResult.hasErrors();
        if(!productService.isUniqueProduct(productCreateDTO.title)){
            model.addAttribute("unique_error", true);
            correct= false;
        }
        if(productCreateDTO.imageFile.getOriginalFilename() == null || productCreateDTO.imageFile.getOriginalFilename().isEmpty()){
            model.addAttribute("null_file", true);
            correct= false;
        }
        return correct;
    }
}
