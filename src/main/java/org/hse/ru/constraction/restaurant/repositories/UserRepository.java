package org.hse.ru.constraction.restaurant.repositories;

import org.hse.ru.constraction.restaurant.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    User findByName(String name);
    User findUserById(Long id);
}
