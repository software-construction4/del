package org.hse.ru.constraction.restaurant.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hse.ru.constraction.restaurant.models.User;
import org.hse.ru.constraction.restaurant.models.enums.Role;
import org.hse.ru.constraction.restaurant.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    public boolean createUser(User user, boolean isAdmin) {
        String email = user.getEmail();
        if(isAdmin){
            user.getRoles().add(Role.ROLE_ADMIN);
        }
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.getRoles().add(Role.ROLE_USER);
        userRepository.save(user);
        log.info("Saving new User with email {}", email);
        return true;
    }

    public boolean isUserUnique(User user){
        return userRepository.findByName(user.getName()) != null;
    }

    public User getUserByPrincipal(Principal principal){
        if (principal == null) return new User();
        return userRepository.findByName(principal.getName());
    }
    public User getUserByID(Long id) {
        return userRepository.findUserById(id);
    }

}
