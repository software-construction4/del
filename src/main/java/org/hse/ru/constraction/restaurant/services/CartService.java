package org.hse.ru.constraction.restaurant.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hse.ru.constraction.restaurant.dto.CartProductDTO;
import org.hse.ru.constraction.restaurant.models.Cart;
import org.hse.ru.constraction.restaurant.repositories.CartRepository;
import org.hse.ru.constraction.restaurant.validator.CartValidator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CartService {
    private final CartRepository cartRepository;
    private final CartProductService cartProductService;
    private final ProductService productService;
    private final CartValidator cartValidator;
    public String createOrUpdateCart(Long productId, Long userId){


        Cart cart = cartRepository.findCartByUserId(userId);
        if(cart == null){
            cart = createCart(userId);
        }
        if(!cartValidator.validateMaxCountProductAddOne(productId,cart.getId())){
            return "Max count product";
        }
        cartProductService.addProduct(productId, cart.getId());
        return "";
    }
    private Cart createCart(Long userId){
        Cart new_cart = new Cart();
        new_cart.setUserId(userId);
        return cartRepository.save(new_cart);
    }
    public Integer getNumberOfProducts(Long userId){
        return cartProductService.getNumberOfProducts(userId);
    }

    public List<CartProductDTO> getProducts(Long userId){

        Cart cart = cartRepository.findCartByUserId(userId);
        if(cart == null){
            cart = createCart(userId);
        }
        return cartProductService.getProducts(cart.getId());
    }

    public void decreaseProduct(Long productId,Long userId){
        Cart cart = cartRepository.findCartByUserId(userId);
        cartProductService.decreaseProduct(productId, cart.getId());
    }

    public void deleteProduct(Long productId, Long userId){
        cartProductService.deleteProductFromCart(productId, cartRepository.findCartByUserId(userId).getId());
    }

    public void updateProduct(Long productId, Long userId, int quantity){
        quantity = cartValidator.validateMaxCountProductUpdateQuantity(productId, quantity);
        Cart cart = cartRepository.findCartByUserId(userId);
        cartProductService.updateProductQuantity(productId, cart.getId(), quantity);

    }

    public Cart getCardIdByUserId(Long userId){
        return cartRepository.findCartByUserId(userId);
    }
    public boolean increaseProduct(Long productId,Long userId){
        Cart cart = cartRepository.findCartByUserId(userId);
        if(!cartValidator.validateMaxCountProductAddOne(productId,cart.getId())){
            return false;
        }
        cartProductService.increaseProduct(productId,cartRepository.findCartByUserId(userId).getId());

        return true;
    }
    public void cleanCart(Long cartId){
        cartProductService.cleanCart(cartId);
    }
}


