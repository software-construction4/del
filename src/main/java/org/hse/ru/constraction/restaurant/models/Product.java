package org.hse.ru.constraction.restaurant.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jdk.jfr.Enabled;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import org.hse.ru.constraction.restaurant.dto.ProductCreateDTO;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "product")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id",columnDefinition = "BIGINT")
    private Long id;

    @NotBlank
    @NotNull
    @Column(name = "title",unique = true)
    private String title;


    @Column(name="description")
    @NotBlank
    @NotNull
    private String description;

    @Column(name = "price")
    @NotNull
    private int price;

    @Column(name = "timeCookingMinutes")
    @NotNull
    private int timeCookingMinutes;

    @Column(name = "isActive" )
    private boolean isActive;



    private LocalDateTime dateOfCreate;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id",nullable = true)
    private Category category;

    @Column(name = "image_url")
    private String imageUrl;
    @PrePersist
    private void init() {
        dateOfCreate = LocalDateTime.now();
        isActive = true;
    }

    public Product(ProductCreateDTO productCreateDTO){
        title = productCreateDTO.title;
        description = productCreateDTO.description;
        price = productCreateDTO.price;
        category = productCreateDTO.category;
        count = productCreateDTO.count;
        timeCookingMinutes= productCreateDTO.getTimeCookingMinutes();
    }

    @NotNull
    private Integer count;

    @OneToMany(mappedBy = "product",fetch = FetchType.LAZY)
    private List<PaidOrderDetails> paidOrderDetails = new LinkedList<>();

    @Override
    public String toString() {
        return Objects.requireNonNullElse(title, "");
    }

}
