package org.hse.ru.constraction.restaurant.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hse.ru.constraction.restaurant.models.Category;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Data
@AllArgsConstructor
public class ProductCreateDTO {
    @NotBlank
    @NotNull
    public String title;

    @NotNull
    public MultipartFile imageFile;

    @NotBlank
    @NotNull
    public String description;

    @NotNull
    public int price;

    @NotNull
    public int timeCookingMinutes;

    public boolean isActive;

    @NotNull
    public Integer count;

    @NotNull
    public Category category;
    public ProductCreateDTO(){}

    @Override
    public String toString() {
        return Objects.requireNonNullElse(title, "");
    }
}
