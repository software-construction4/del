package org.hse.ru.constraction.restaurant.repositories;

import jakarta.transaction.Transactional;
import org.hse.ru.constraction.restaurant.dto.CartProductDTO;
import org.hse.ru.constraction.restaurant.models.Cart;
import org.hse.ru.constraction.restaurant.models.CartProduct;
import org.hse.ru.constraction.restaurant.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.Objects;

public interface CartProductsRepository extends JpaRepository<CartProduct, Long> {
    @Query("select cp from CartProduct cp where cp.product_id = ?1 and cp.cart_id = ?2")
    CartProduct productExistsInCart(Long productId, Long cartId);

    @Query("select sum(cp.count) from CartProduct cp where cp.product_id = ?1 ")
    Integer productCurrentCount(Long productId);

    @Query("select count(cp) from CartProduct cp where (select c.userId from Cart c where c.id = cp.cart_id) = ?1")
    Integer numberOfProductsByUserId(Long userId);

    @Query("select new org.hse.ru.constraction.restaurant.dto.CartProductDTO(p.title,p.category.name,cp.count,p.price,p.imageUrl) from CartProduct cp join Product p on cp.product_id = p.id where cp.cart_id = ?1 ")
    List<CartProductDTO> productByCartId(Long cartId);

    @Query("select cp from CartProduct cp where cp.product_id = ?1 and cp.cart_id = ?2")
    CartProduct findCartProductByProduct_idAndCart_id(Long productId, Long categoryId);

    @Modifying
    @Transactional
    @Query("delete from CartProduct cp where cp.product_id = ?1")
    void deleteRelatedProducts(Long productId);

    @Modifying
    @Transactional
    @Query("delete from CartProduct cp where cp.cart_id = ?1 and cp.product_id = ?2")
    void deleteByCart_idAndProduct_id(Long cartId, Long productId);

    @Modifying
    @Transactional
    @Query("delete from CartProduct cp where cp.cart_id = ?1")
    void deleteByCart_id(Long cartId);

    @Query("select cp from CartProduct cp where cp.cart_id = ?1")
    List<CartProduct> getCartProductByCart_id(Long cartId);
}
