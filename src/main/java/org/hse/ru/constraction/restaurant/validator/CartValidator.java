package org.hse.ru.constraction.restaurant.validator;

import lombok.RequiredArgsConstructor;
import org.hse.ru.constraction.restaurant.models.CartProduct;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.services.CartProductService;
import org.hse.ru.constraction.restaurant.services.ProductService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartValidator {
    private final ProductService productService;
    private final CartProductService cartProductService;

    public Boolean validateMaxCountProductAddOne(Long productId, Long cartId) {
        Product product = productService.getProductById(productId);

        CartProduct cartProduct = cartProductService.getCartProduct(productId, cartId);
        if (cartProduct == null) {
            return 1 <= product.getCount();
        }
        return cartProduct.getCount() + 1 <= product.getCount();
    }


    public int validateMaxCountProductUpdateQuantity(Long productId, int quantity) {
        Product product = productService.getProductById(productId);
        if (quantity > product.getCount()) {
            quantity = product.getCount();
        }
        return quantity;
    }

}
