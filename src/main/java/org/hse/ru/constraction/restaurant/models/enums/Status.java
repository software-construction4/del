package org.hse.ru.constraction.restaurant.models.enums;

public enum Status {
    CART,
    ACCEPTED,
    COOKING,
    READY,
    PAID
}
