package org.hse.ru.constraction.restaurant.repositories;

import org.hse.ru.constraction.restaurant.models.Category;
import org.hse.ru.constraction.restaurant.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Objects;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
