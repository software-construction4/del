package org.hse.ru.constraction.restaurant.services;

import io.imagekit.sdk.ImageKit;
import io.imagekit.sdk.config.Configuration;
import io.imagekit.sdk.exceptions.*;
import io.imagekit.sdk.models.FileCreateRequest;
import io.imagekit.sdk.models.results.Result;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

@Service
public class UploadImageService {
    public List<String> results = new LinkedList<>() {
    };

    private ImageKit imageKit = ImageKit.getInstance();

    @Value("${publicKey}")
    private String publicKey;

    @Value("${privateKey}")
    private String privateKey;

    @Value("${outPointUrl}")
    private String outPointUrl;



    public String uploadImage(MultipartFile file) throws ForbiddenException, TooManyRequestsException, InternalServerException, UnauthorizedException, BadRequestException, UnknownException, IOException {
        imageKit.setConfig(new Configuration(publicKey, privateKey, outPointUrl));
        FileCreateRequest fileCreateRequest =new FileCreateRequest(new InputStreamResource(new ByteArrayInputStream(file.getBytes())).getContentAsByteArray(), file.getName());
        List<String> responseFields=new ArrayList<>();
        responseFields.add("thumbnail");
        responseFields.add("tags");
        responseFields.add("customCoordinates");
        fileCreateRequest.setResponseFields(responseFields);
        List<String> tags=new ArrayList<>();
        tags.add("Software");
        tags.add("Developer");
        tags.add("Engineer");
        fileCreateRequest.setTags(tags);
        Result result=ImageKit.getInstance().upload(fileCreateRequest);
        results.add(result.getUrl());
        return result.getUrl();
    }
}
