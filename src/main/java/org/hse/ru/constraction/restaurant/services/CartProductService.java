package org.hse.ru.constraction.restaurant.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hse.ru.constraction.restaurant.dto.CartProductDTO;
import org.hse.ru.constraction.restaurant.models.Cart;
import org.hse.ru.constraction.restaurant.models.CartProduct;
import org.hse.ru.constraction.restaurant.repositories.CartProductsRepository;
import org.hse.ru.constraction.restaurant.repositories.CartRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CartProductService {
    private final CartProductsRepository cartProductsRepository;

    public Integer getProductCurrentNumber(Long productId) {
        Integer count = cartProductsRepository.productCurrentCount(productId);
        if(count == null){
            return 0;
        }
        return count;
    }


    public void updateProductQuantity(Long productId, Long cartId, int newQuantity){
        CartProduct cartProduct = cartProductsRepository.findCartProductByProduct_idAndCart_id(productId, cartId);
        if(newQuantity <=0){
            deleteProductFromCart(productId, cartId);
        } else {
            cartProduct.setCount(newQuantity);
            cartProductsRepository.save(cartProduct);
        }
    }

    public CartProduct getCartProduct(Long productId, Long cartId){
        return cartProductsRepository.findCartProductByProduct_idAndCart_id(productId, cartId);
    }

    public void addProduct(Long productId, Long cartId){
        CartProduct product = cartProductsRepository.productExistsInCart(productId, cartId);

        if(product == null){
            CartProduct newCart = new CartProduct();
            newCart.setProduct_id(productId);
            newCart.setCart_id(cartId);
            newCart.setCount(1);
            cartProductsRepository.save(newCart);
        } else {
            product.setCount(product.getCount() + 1);
            cartProductsRepository.save(product);
        }

    }
    public List<CartProductDTO> getProducts(Long cartId) {
        return  cartProductsRepository.productByCartId(cartId);
    }

    public List<CartProduct> getCartProductsByCartId(Long cartId) {
        return cartProductsRepository.getCartProductByCart_id(cartId);
    }
    public void deleteRelatedProducts(Long productId){
        cartProductsRepository.deleteRelatedProducts(productId);
    }
    public Integer getNumberOfProducts(Long userId){
        return cartProductsRepository.numberOfProductsByUserId(userId);
    }

    public void deleteProductFromCart(Long productId, Long cartId){
        cartProductsRepository.deleteByCart_idAndProduct_id(cartId,productId);
    }

    public void getCartProductByUserId(Long userId){

    }
    public void decreaseProduct(Long productId, Long cartId){
        CartProduct cartProduct = cartProductsRepository.findCartProductByProduct_idAndCart_id(productId, cartId);
        updateProductQuantity(productId,cartId,cartProduct.getCount() -1 );
    }

    public void cleanCart(Long cartId){
        cartProductsRepository.deleteByCart_id(cartId);
    }
    public void increaseProduct(Long productId, Long cartId){
        CartProduct cartProduct = cartProductsRepository.findCartProductByProduct_idAndCart_id(productId, cartId);
        updateProductQuantity(productId,cartId,cartProduct.getCount() + 1 );
    }
}




