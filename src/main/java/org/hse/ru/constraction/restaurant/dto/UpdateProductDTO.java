package org.hse.ru.constraction.restaurant.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateProductDTO {
    private Long id;


    @NotNull
    private Integer count;

    @Column(name = "price")
    @NotNull
    private int price;

    @NotNull
    private int timeCookingMinutes;

}
