package org.hse.ru.constraction.restaurant.controllers;

import io.imagekit.sdk.exceptions.*;
import jakarta.persistence.Query;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.hse.ru.constraction.restaurant.dto.ProductCreateDTO;
import org.hse.ru.constraction.restaurant.dto.UpdateProductDTO;
import org.hse.ru.constraction.restaurant.models.Category;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.services.CategoryService;
import org.hse.ru.constraction.restaurant.services.ProductService;
import org.hse.ru.constraction.restaurant.services.UploadImageService;
import org.hse.ru.constraction.restaurant.services.UserService;
import org.hse.ru.constraction.restaurant.validator.ProductValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final UserService userService;
    private final CategoryService categoryService;
    private final ProductValidator productValidator;
    private final UploadImageService uploadImageService;
    @GetMapping("/")
    public String products(Model model, Principal principal) {

        List<Category> categories = categoryService.getAllCategories();
        model.addAttribute("categories", categories);
        model.addAttribute("user", userService.getUserByPrincipal(principal));

        return "layout";
    }

    @GetMapping("/product/create")
    public String createProductGet(Model model) {
        model.addAttribute("product", new ProductCreateDTO());
        return "add-product";
    }


    @GetMapping("/product/update")
    public String updateProductUpdate(@RequestParam("id") Long productId, Model model){
         Product product = productService.getProductById(productId);
         UpdateProductDTO updateProduct = new UpdateProductDTO(productId,product.getCount(),product.getPrice(),product.getTimeCookingMinutes());
         model.addAttribute("product", updateProduct);
         model.addAttribute("productName", product.getTitle());
         return "update-product";
    }


    @PostMapping("/product/update")
    public String updateProduct(@Valid UpdateProductDTO updateProductDTO, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "update-product";
        }
        productService.updateProduct(updateProductDTO);
        return "redirect:/";
    }

    @PostMapping("/product/create")
    public String createProduct(@Valid @ModelAttribute("product") ProductCreateDTO product, BindingResult bindingResult, Model model) throws ForbiddenException, TooManyRequestsException, InternalServerException, UnauthorizedException, BadRequestException, UnknownException, IOException {

        if(!productValidator.validateProduct(model,bindingResult,product)){
            return "add-product";
        }

        if (!productService.createProduct(product)) {
            model.addAttribute("creation_error", true);
            return "add-product";
        }

        return "redirect:/";
    }

    @PostMapping("/product/delete")
    public String deleteProduct(@RequestParam("id") Long productId){
        productService.deleteProduct(productId);

        return "redirect:/";
    }


}
