package org.hse.ru.constraction.restaurant.schedule;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hse.ru.constraction.restaurant.models.PaidOrder;
import org.hse.ru.constraction.restaurant.models.enums.Status;
import org.hse.ru.constraction.restaurant.services.PaidOrderDetailsService;
import org.hse.ru.constraction.restaurant.services.PaidOrderService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class UpdateOrderStatus {
    private final PaidOrderService orderService;

    @Value("${maxOrdersCount}")
    private final Integer maxOrdersCount=0;
    private Boolean checkTime(PaidOrder order){
        return ChronoUnit.MINUTES.between(order.getStartedTime(),LocalDateTime.now()) > order.getTotalTimCooking();

    }
    @Scheduled(fixedDelay = 30)
    @Transactional
    public void updateOrderStatus(){

        List<PaidOrder> acceptedOrders = orderService.getOrdersWithStatus(Status.ACCEPTED);
        List<PaidOrder> cookingOrders = orderService.getOrdersWithStatus(Status.COOKING);
        acceptedOrders.sort(Comparator.comparing(PaidOrder::getCreatedTime));

        for(int i = 0; i < Math.min(maxOrdersCount - cookingOrders.size(),acceptedOrders.size()); i++){

            acceptedOrders.get(i).setStatus(Status.COOKING);
            acceptedOrders.get(i).setStartedTime(LocalDateTime.now());
            orderService.saveOrderDetails(acceptedOrders.get(i));
            log.info("Change Order Status to COOKING {}", acceptedOrders.get(i).getUniqueNumberOfOrder());
        }
        for(PaidOrder order :cookingOrders){

            if(checkTime(order)){
                order.setStatus(Status.READY);
                log.info("Change Order Status to READY {}", order.getUniqueNumberOfOrder());
            }
        }


    }
}
