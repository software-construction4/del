package org.hse.ru.constraction.restaurant.repositories;

import jakarta.transaction.Transactional;
import org.hse.ru.constraction.restaurant.dto.OrderDetailDTO;
import org.hse.ru.constraction.restaurant.models.CartProduct;
import org.hse.ru.constraction.restaurant.models.PaidOrder;
import org.hse.ru.constraction.restaurant.models.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PaidOrderRepository   extends JpaRepository<PaidOrder, Long> {
    PaidOrder findByUniqueNumberOfOrder(String orderNumber);


    @Modifying
    @Transactional
    @Query("select ord from PaidOrder ord  where ord.userId = ?1 ")
    List<PaidOrder> getOrdersByUserId(Long userId);

    @Query("select ord from PaidOrder ord where ord.status = ?1 ")
    List<PaidOrder> getOrdersWithStatus(Status status);

}

