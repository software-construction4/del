package org.hse.ru.constraction.restaurant.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hse.ru.constraction.restaurant.exception.InvalidCountException;
import org.hse.ru.constraction.restaurant.models.Cart;
import org.hse.ru.constraction.restaurant.models.PaidOrder;
import org.hse.ru.constraction.restaurant.models.PaidOrderDetails;
import org.hse.ru.constraction.restaurant.models.enums.Status;
import org.hse.ru.constraction.restaurant.repositories.PaidOrderRepository;
import org.hse.ru.constraction.restaurant.validator.OrderValidator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class PaidOrderService {
    private final CartService cartService;
    private final UniqueNumberGenerator uniqueNumberGenerator;
    private final OrderValidator validator;
    private final PaidOrderRepository paidOrderRepository;
    private final PaidOrderDetailsService paidOrderDetailsService;
    public void createOrder(Long userId) throws InvalidCountException {
        String uniqueNumber;
        Cart cart = cartService.getCardIdByUserId(userId);
        if(cart == null || cartService.getNumberOfProducts(userId)==0){
            return;
        }
        do {
            uniqueNumber =  uniqueNumberGenerator.generateRandomNumberOfOrder();
        }while (!validator.validateUniqueOrderNumber(uniqueNumber));

        if(!validator.validateMaxCountProduct(cart.getId())){
            throw new InvalidCountException("К сожалению, такое кол-во товара отсутствует. Измените количество блюд." );
        }
        PaidOrder paidOrder = PaidOrder.builder()
                .userId(cart.getUserId())
                .status(Status.ACCEPTED)
                .createdTime(LocalDateTime.now())
                .uniqueNumberOfOrder(uniqueNumber)
                .build();
        PaidOrder savedOrder =  paidOrderRepository.save(paidOrder);
        log.info("Create new Order {}", savedOrder.getUniqueNumberOfOrder());
        paidOrderDetailsService.transferDataFromCartToOrder(savedOrder,cart);
        cartService.cleanCart(cart.getId());

    }
    public List<PaidOrder> getOrdersWithStatus(Status status) {
        return paidOrderRepository.getOrdersWithStatus(status);
    }

    public void saveOrderDetails(PaidOrder orderDetails){
        paidOrderRepository.save(orderDetails);
    }

    private Optional<PaidOrder> getOrderById(Long orderId) {
        return paidOrderRepository.findById(orderId);
    }
    public void cancelOrder(Long orderId){
        Optional<PaidOrder> order = getOrderById(orderId);
        order.ifPresent(paidOrderRepository::delete);
    }
    public void payOrder(Long orderId){
        Optional<PaidOrder> order = getOrderById(orderId);
        if(order.isPresent()) {
            order.get().setStatus(Status.PAID);
            paidOrderRepository.save(order.get());
        }
    }
    public List<PaidOrder> getAllOrdersByUserId(Long userId) {
        return paidOrderRepository.getOrdersByUserId(userId);
    }
}

