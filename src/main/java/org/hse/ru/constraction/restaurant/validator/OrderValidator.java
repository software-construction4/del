package org.hse.ru.constraction.restaurant.validator;

import lombok.RequiredArgsConstructor;
import org.hse.ru.constraction.restaurant.models.Cart;
import org.hse.ru.constraction.restaurant.models.CartProduct;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.repositories.PaidOrderRepository;
import org.hse.ru.constraction.restaurant.services.CartProductService;
import org.hse.ru.constraction.restaurant.services.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderValidator {
    private final PaidOrderRepository paidOrderRepository;
    private final ProductService productService;
    private final CartProductService cartProductService;
    public boolean validateUniqueOrderNumber(String orderNumber){
        return paidOrderRepository.findByUniqueNumberOfOrder(orderNumber)==null;
    }


    public Boolean validateMaxCountProduct(Long cardId){
        List<CartProduct> cartProducts = cartProductService.getCartProductsByCartId(cardId);
        for(CartProduct cartProduct : cartProducts) {
            if (cartProduct.getCount() > productService.getProductById(cartProduct.getProduct_id()).getCount()) {
                return false;
            }
        }
        return true;
    }


}
