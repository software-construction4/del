package org.hse.ru.constraction.restaurant.controllers;



import jakarta.persistence.Query;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.hse.ru.constraction.restaurant.dto.OrderDetailDTO;
import org.hse.ru.constraction.restaurant.dto.UpdateProductDTO;
import org.hse.ru.constraction.restaurant.exception.InvalidCountException;
import org.hse.ru.constraction.restaurant.models.Category;
import org.hse.ru.constraction.restaurant.models.PaidOrder;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.models.User;
import org.hse.ru.constraction.restaurant.repositories.PaidOrderRepository;
import org.hse.ru.constraction.restaurant.services.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class OrderController {
    private final UserService userService;
    private final CartService cartService;
    private final PaidOrderService orderService;
    private final ProductController productController;
    private final PaidOrderRepository orderRepository;
    @GetMapping("/order")
    public String getOrders(Model model, Principal principal) {

        User user = userService.getUserByPrincipal(principal);
        List<PaidOrder> list = orderService.getAllOrdersByUserId(user.getId());
        list.sort((o1, o2) -> o2.getCreatedTime().compareTo(o1.getCreatedTime()));
        model.addAttribute("orders", list);
        return "orders";
    }

    @PostMapping("/order/create")
    public String createOrder(Principal principal, RedirectAttributes redirectAttributes){
        User user = userService.getUserByPrincipal(principal);
        try {
            orderService.createOrder(user.getId());
        }catch (InvalidCountException exception){

            redirectAttributes.addFlashAttribute("error", exception.getMessage());
            return "redirect:/cart";
        }
        return "redirect:/order";

    }
    @PostMapping("/order/cancel")
    public String cancelOrder(Principal principal, @RequestParam("orderId") Long orderId){
        orderService.cancelOrder(orderId);

        return "redirect:/order";
    }
    @PostMapping("/order/paid")
    public String payOrder(Principal principal, @RequestParam("orderId") Long orderId){
        orderService.payOrder(orderId);

        return "redirect:/order";
    }




}
