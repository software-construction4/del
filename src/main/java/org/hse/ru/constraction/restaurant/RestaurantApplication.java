package org.hse.ru.constraction.restaurant;

import io.imagekit.sdk.ImageKit;
import io.imagekit.sdk.config.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RestaurantApplication {

	public static void main(String[] args) {
		ImageKit imageKit = ImageKit.getInstance();
		Configuration config = new Configuration("public_XBlAmPZWneS7YxH5X7qv3pMjnI8=", "private_VSrEtydzNkFonei2YP+5luD9wDw=", "https://ik.imagekit.io/n1ccwsxwg/");
		imageKit.setConfig(config);
		SpringApplication.run(RestaurantApplication.class, args);
	}

}

