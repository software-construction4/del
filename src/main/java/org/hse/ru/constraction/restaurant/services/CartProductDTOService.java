package org.hse.ru.constraction.restaurant.services;

import org.hse.ru.constraction.restaurant.dto.CartProductDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartProductDTOService {
    public int getTotalSum(List<CartProductDTO> productDTOS){
        return  productDTOS.stream()
                .mapToInt(cp -> cp.getPrice() * cp.getCount())
                .sum();
    }
}
