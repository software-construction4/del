package org.hse.ru.constraction.restaurant.controllers;


import lombok.RequiredArgsConstructor;
import org.hse.ru.constraction.restaurant.dto.CartProductDTO;
import org.hse.ru.constraction.restaurant.models.Product;
import org.hse.ru.constraction.restaurant.services.CartProductDTOService;
import org.hse.ru.constraction.restaurant.services.CartService;
import org.hse.ru.constraction.restaurant.services.ProductService;
import org.hse.ru.constraction.restaurant.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.hse.ru.constraction.restaurant.models.User;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;

@Controller
@RequiredArgsConstructor

public class CartController {
    private final UserService userService;
    private final CartService cartService;
    private final CartProductDTOService cartProductDTOService;
    private final ProductService productService;

    @PostMapping("/cart/add-product")
    public String addProductToCart(@RequestParam("id") Long productId, Principal principal) {
        User user = userService.getUserByPrincipal(principal);
        cartService.createOrUpdateCart(productId, user.getId());
        return "redirect:/";
    }

    @GetMapping("/cart")
    public String getCart(Principal principal, Model model, @ModelAttribute("error") String error) {
        User user = userService.getUserByPrincipal(principal);
        List<CartProductDTO> productDTOS = cartService.getProducts(user.getId());
        productDTOS.sort(Comparator.comparing(CartProductDTO::getName));
        model.addAttribute("error", error);
        model.addAttribute("products", productDTOS);
        model.addAttribute("totalSum", cartProductDTOService.getTotalSum(productDTOS));
        model.addAttribute("itemNumber", productDTOS.toArray().length);
        return "cart";
    }

    @PostMapping("/cart/subst")
    public String decreaseProductNumber(@RequestParam("productName") String productName, Principal principal) {
        Product product = productService.getProductByTitle(productName);
        User user = userService.getUserByPrincipal(principal);
        cartService.decreaseProduct(product.getId(), user.getId());
        return "redirect:/cart";
    }

    @PostMapping("/cart/delete")
    public String deleteProduct(@RequestParam("productName") String productName, Principal principal) {
        cartService.deleteProduct(productService.getProductByTitle(productName).getId(),
                userService.getUserByPrincipal(principal).getId());
        return "redirect:/cart";
    }

    @PostMapping("/cart/update")
    public String updateProduct(@RequestParam("quantity") int quantity,@RequestParam("productName") String productName, Principal principal) {
        cartService.updateProduct(productService.getProductByTitle(productName).getId(), userService.getUserByPrincipal(principal).getId(), quantity);
        return "redirect:/cart";
    }

    @PostMapping("/cart/increase")
    public String increaseProduct(@RequestParam("productName") String productName, Principal principal) {
        cartService.increaseProduct(productService.getProductByTitle(productName).getId(), userService.getUserByPrincipal(principal).getId());
        return "redirect:/cart";
    }
}
