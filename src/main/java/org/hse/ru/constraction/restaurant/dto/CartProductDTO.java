package org.hse.ru.constraction.restaurant.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartProductDTO {
    public String name;
    public String category;
    public int count;
    public int price;
    public String imageUrl;
}
