package org.hse.ru.constraction.restaurant.repositories;

import jakarta.transaction.Transactional;
import org.hse.ru.constraction.restaurant.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;


public interface ProductRepository  extends JpaRepository<Product, Long> {
    @Transactional
    @Query("select p from Product p where p.title = ?1")
    Product findByTitle(String title);


}
