let imageId =  document.querySelector("#imageId")
console.log(imageId)
console.log(imageId.dataset.imageid)
if(imageId.dataset.imageid != null){
    console.log(imageId.dataset.imageid)
    fetch('/images/' + imageId.dataset.imageid)
        .then(response => response.blob())
        .then(blob => {
            let file = new File([], imageId.dataset.imageid, {type: 'image/jpeg'});
            let reader = new FileReader();
            reader.onloadend = () => {
                file.lastModifiedDate = new Date();
                file.name = imageId.dataset.imageid;
                file.size = reader.result.byteLength;
                file.type = 'image/jpeg';
                let list = new DataTransfer();
                list.items.add(file)

                let input = document.getElementById('imageProduct');
                input.files = list.files
            };
            reader.readAsArrayBuffer(blob);
        });
}


var imagekit = new ImageKit({
    publicKey : "public_XBlAmPZWneS7YxH5X7qv3pMjnI8=",
    urlEndpoint : "https://ik.imagekit.io/n1ccwsxwg",
    authenticationEndpoint : "http://www.yourserver.com/auth",
});

// URL generation
var imageURL = imagekit.url({
    path : "/default-image.jpg",
    transformation : [{
        "height" : "300",
        "width" : "400"
    }]
});

// Upload function internally uses the ImageKit.io javascript SDK
function upload() {
    var file = document.getElementById("imageProduct");
    imagekit.upload({
        file : file.files[0],
        fileName : "abc1.jpg",
        tags : ["tag1"]
    }, function(err, result) {
        console.log(arguments);
        console.log(imagekit.url({
            src: result.url,
            transformation : [{ height: 300, width: 400}]
        }));
    })
}