let quantities = document.querySelectorAll(".update-quantity-form")
quantities.forEach(quantity =>{
    quantity.addEventListener("change",function (event){
        quantity.submit();
    })
})

const toastError = document.getElementById('error')
window.onload = () =>{
    console.log(toastError)
    if(toastError != null){
        const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastError)

        toastBootstrap.show();
    }

}
